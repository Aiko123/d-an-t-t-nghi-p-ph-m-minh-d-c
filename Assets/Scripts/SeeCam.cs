﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeCam : MonoBehaviour
{
    private Camera mainCamera;

    void Start()
    {
        // Lấy tham chiếu đến camera chính
        mainCamera = Camera.main;
    }

    void LateUpdate()
    {
        // Kiểm tra xem camera đã được thiết lập chưa
        if (mainCamera != null)
        {
            // Lấy vector hướng từ đối tượng đến camera
            Vector3 directionToCamera = mainCamera.transform.position - transform.position;

            // Không làm thay đổi góc quay theo trục y
            directionToCamera.y = 0;

            // Quay đối tượng để hướng về camera
            transform.rotation = Quaternion.LookRotation(directionToCamera);
        }
        else
        {
            Debug.LogWarning("Không tìm thấy camera chính trong scene.");
        }
    }
}
