﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHandle : MonoBehaviour
{
    public GameObject prefabToReceive;

    public void SetPrefabToReceive(GameObject prefab)
    {
        prefabToReceive = prefab;
    }

    public void SendPrefabToOtherScene()
    {
        if (prefabToReceive != null)
        {
            // Lấy PrefabReceiver instance từ scene hiện tại
            PrefabReceiver prefabReceiver = FindObjectOfType<PrefabReceiver>();
            if (prefabReceiver != null)
            {
                // Gọi phương thức ReceivePrefab trên PrefabReceiver instance và truyền prefab cần gửi
                prefabReceiver.ReceivePrefab(prefabToReceive);
                Debug.Log("Prefab sent to other scene: " + prefabToReceive.name);
            }
            else
            {
                Debug.LogWarning("PrefabReceiver instance not found in current scene.");
            }
        }
        else
        {
            Debug.LogWarning("No prefab selected to send.");
        }
    }
}
