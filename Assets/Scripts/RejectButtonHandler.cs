﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RejectButtonHandler : MonoBehaviour
{
    public void RejectAndResetPrefabs()
    {
        // Tìm và lấy PrefabReceiver instance trong Scene hiện tại
        PrefabReceiver prefabReceiver = FindObjectOfType<PrefabReceiver>();

        // Nếu tìm thấy PrefabReceiver instance, hủy nó
        if (prefabReceiver != null)
        {
            Destroy(prefabReceiver.gameObject);
            Debug.Log("PrefabReceiver instance destroyed.");
        }

        // Load lại Scene chính
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Debug.Log("Scene reloaded.");
    }
}
