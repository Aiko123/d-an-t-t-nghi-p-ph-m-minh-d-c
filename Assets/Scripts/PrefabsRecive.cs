﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabReceiver : MonoBehaviour
{
    public static PrefabReceiver instance; // Singleton instance

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject); // Đảm bảo GameObject này không bị hủy khi chuyển scene
        }
        else
        {
            Destroy(gameObject); // Nếu đã tồn tại instance khác, hủy GameObject này đi
        }
    }

    public void ReceivePrefab(GameObject prefab)
    {
        // Instantiate prefab và đặt là con của GameObject này
        GameObject instantiatedPrefab = Instantiate(prefab, transform.position, Quaternion.identity);
        instantiatedPrefab.transform.parent = transform;
    }
}
