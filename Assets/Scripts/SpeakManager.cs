﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class SpeakManager : MonoBehaviour
{
    public Button speakButton; // Nút để ẩn/hiện đối tượng
    public GameObject objectSpawner; // GameObject cha chứa các loại đối tượng cần kiểm tra
    public string[] objectNamesToCheck; // Tên của 7 loại GameObject bạn quan tâm
    private bool areObjectsVisible = false; // Trạng thái hiện tại của các đối tượng

    void Start()
    {
        // Thêm sự kiện cho nút button
        speakButton.onClick.AddListener(SpeakObjectVisibility);
    }

    // Hàm kiểm tra và ẩn/hiện đối tượng khi nhấn nút
    public void SpeakObjectVisibility()
    {
        // Kiểm tra xem objectSpawner có tồn tại không
        if (objectSpawner != null)
        {
            // Duyệt qua các tên GameObject để tìm và ẩn/hiện các đối tượng Quad
            foreach (string objectName in objectNamesToCheck)
            {
                Transform objectToCheck = objectSpawner.transform.Find(objectName);
                if (objectToCheck != null)
                {
                    SpeakVisibility(objectToCheck.gameObject);
                }
                else
                {
                    Debug.Log("Không tìm thấy GameObject có tên " + objectName + " trong " + objectSpawner.name);
                }
            }

            // Đảo ngược trạng thái của các đối tượng
            areObjectsVisible = !areObjectsVisible;
        }
        else
        {
            Debug.Log("objectSpawner không được thiết lập. Vui lòng gán đối tượng cha chứa các loại đối tượng cần kiểm tra.");
        }
    }

    // Hàm ẩn/hiện đối tượng Speak và kích hoạt animation
    private void SpeakVisibility(GameObject obj)
    {
        Transform quadTransform = obj.transform.Find("Speak"); // Tìm đối tượng Speak trong GameObject
        if (quadTransform != null)
        {
            // Đảo ngược trạng thái của đối tượng Speak
            quadTransform.gameObject.SetActive(!areObjectsVisible);

            // Lấy Animator từ đối tượng cha của đối tượng Speak
            Animator anim = quadTransform.GetComponentInParent<Animator>();
            if (anim != null)
            {
                // Đảo ngược trạng thái của biến Speak trong Animator
                bool m_speak = anim.GetBool("Speak");
                anim.SetBool("Speak", !m_speak); // Đặt giá trị mới cho biến Speak trong Animator
            }
        }
        else
        {
            Debug.Log("Không tìm thấy đối tượng Speak trong " + obj.name);
        }
    }
}
