﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovementAndCollision : MonoBehaviour
{
    public float moveSpeed = 1f; // Tốc độ di chuyển của đối tượng
    public float collisionRadius = 0.5f; // Bán kính va chạm để xác định khi nào di chuyển kết thúc
    private bool isMoving = false; // Biến để kiểm tra xem đối tượng có đang di chuyển hay không
    private Animator raccoonAnimator; // Animator của Raccoon

    public GameObject targetPrefab; // Prefab được spawn
    private Vector3 targetPosition; // Vị trí mục tiêu
    private bool moveToCamera = false; // Biến để kiểm tra xem có đang di chuyển đến Camera hay không
    public Camera arCamera; // Biến để lưu trữ AR Camera

    private void Start()
    {
        // Tìm và lưu animator của Raccoon
        Transform raccoon = transform.Find("PrefabReceiver/Raccoon");
        if (raccoon != null)
        {
            raccoonAnimator = raccoon.GetComponent<Animator>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Nếu đối tượng đang di chuyển, thực hiện hành động di chuyển
        if (isMoving)
        {
            MoveToTarget();
        }
        else if (moveToCamera)
        {
            MoveTowardsCameraTarget();
        }
    }

    // Hàm di chuyển đến mục tiêu
    void MoveToTarget()
    {
        // Tính toán hướng và khoảng cách đến mục tiêu
        Vector3 direction = targetPosition - transform.position;
        float distance = direction.magnitude;

        // Nếu khoảng cách nhỏ hơn bán kính va chạm, ngừng di chuyển và kích hoạt va chạm
        if (distance < collisionRadius)
        {
            isMoving = false;
            OnCollisionWithTarget();
            return;
        }

        // Quay đầu đối tượng về phía hướng di chuyển
        transform.LookAt(targetPosition);

        // Di chuyển theo hướng với tốc độ đã chỉ định
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);

        // Kích hoạt animation Walk của Raccoon nếu có
        if (raccoonAnimator != null)
        {
            //raccoonAnimator.SetTrigger("GetUp");
            raccoonAnimator.SetBool("Walk", true);
        }
    }

    // Hàm xử lý khi đối tượng va chạm với mục tiêu
    void OnCollisionWithTarget()
    {
        // Destroy đối tượng được spawn (Prefab)
        //Destroy(targetPrefab);

        // Tắt animation Walk của Raccoon
        if (raccoonAnimator != null)
        {
            raccoonAnimator.SetBool("Walk", false);
        }

        // Kích hoạt animation Eat của Raccoon
        if (raccoonAnimator != null)
        {
            raccoonAnimator.SetTrigger("Eat");
        }

        // Sau 4 giây, tắt animation Eat
        Invoke("DisableEatAnimation", 4f);
    }

    // Hàm để tắt animation Eat sau khi thời gian được chỉ định
    void DisableEatAnimation()
    {
        if (raccoonAnimator != null)
        {
            raccoonAnimator.ResetTrigger("Eat");
        }
    }

    // Hàm để bắt đầu di chuyển đến mục tiêu
    public void StartMovingToTarget(GameObject prefab)
    {
        targetPrefab = prefab;
        targetPosition = targetPrefab.transform.position;
        isMoving = true;
    }

    // Phương thức để kích hoạt animation Spin khi bấm nút
    public void TriggerSpin()
    {
        if (raccoonAnimator != null)
        {
            raccoonAnimator.SetBool("Spin", true);
            Invoke("DisableSpin", 5f);
        }
    }

    // Phương thức để tắt animation Spin sau 5 giây
    void DisableSpin()
    {
        if (raccoonAnimator != null)
        {
            raccoonAnimator.SetBool("Spin", false);
        }
    }

    // Phương thức để di chuyển đối tượng lại gần vị trí của AR Camera
    public void MoveTowardsARCamera()
    {
        // Tìm AR Camera trong đối tượng AR Session Origin
        GameObject arSessionOrigin = GameObject.Find("AR Session Origin");
        if (arSessionOrigin != null)
        {
            Transform arCameraTransform = arSessionOrigin.transform.Find("AR Camera");
            if (arCameraTransform != null)
            {
                arCamera = arCameraTransform.GetComponent<Camera>();
                if (arCamera != null)
                {
                    //MoveTowardsCameraTarget();
                    targetPosition = arCamera.transform.position + arCamera.transform.forward * 2f; // Di chuyển tới trước camera 2 đơn vị
                    moveToCamera = true;
                }
                else
                {
                    Debug.LogWarning("AR Camera component not found.");
                }
            }
            else
            {
                Debug.LogWarning("AR Camera object not found in AR Session Origin.");
            }
        }
        else
        {
            Debug.LogWarning("AR Session Origin object not found.");
        }
    }

    // Hàm di chuyển từ từ đến vị trí Camera
    void MoveTowardsCameraTarget()
    {
        // Tính toán hướng và khoảng cách đến vị trí camera
        Vector3 direction = targetPosition - transform.position;
        float distance = direction.magnitude;

        // Giữ nguyên giá trị Y hiện tại của đối tượng
        direction.y = 0;

        // Nếu khoảng cách nhỏ hơn bán kính va chạm, ngừng di chuyển
        if (distance < collisionRadius)
        {
            StopMovingToCamera();
            return;
        }

        // Quay đầu đối tượng về phía hướng di chuyển
        transform.LookAt(new Vector3(targetPosition.x, transform.position.y, targetPosition.z));

        // Di chuyển theo hướng với tốc độ đã chỉ định
        transform.Translate(direction.normalized * moveSpeed * Time.deltaTime, Space.World);

        // Kích hoạt animation Walk của Raccoon nếu có
        if (raccoonAnimator != null)
        {
            raccoonAnimator.SetTrigger("GetUp");
            raccoonAnimator.SetBool("Walk", true);
        }
    }

    // Hàm dừng di chuyển tới vị trí camera
    void StopMovingToCamera()
    {
        moveToCamera = false;
        // Tắt animation Walk của Raccoon
        if (raccoonAnimator != null)
        {
            raccoonAnimator.SetBool("Walk", false);
        }
        Debug.Log("Reached AR Camera position and stopped moving.");
    }
}
