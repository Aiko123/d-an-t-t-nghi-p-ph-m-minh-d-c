﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeNam : MonoBehaviour
{
    private string[] validNames = { "Squid_Animations(Clone)", "Pudu_Animations(Clone)", "Muskrat_Animations(Clone)", "Herring_Animations(Clone)", "Colobus_Animations(Clone)" };

    private void Start()
    {
        // Tìm đối tượng con của đối tượng Pet (PrefabReceiver)
        Transform petChild = transform.Find("PrefabReceiver");

        // Kiểm tra xem có tìm thấy đối tượng con không
        if (petChild != null)
        {
            // Duyệt qua mỗi tên trong mảng validNames
            foreach (string name in validNames)
            {
                // Tìm đối tượng con của đối tượng PrefabReceiver với tên trong mảng validNames
                Transform child = petChild.Find(name);
                if (child != null)
                {
                    // Nếu tìm thấy, đổi tên thành "Raccoon"
                    child.name = "Raccoon";
                    // Sau khi tìm thấy một đối tượng, thoát khỏi vòng lặp
                    break;
                }
            }
        }
        else
        {
            Debug.LogWarning("PrefabReceiver not found.");
        }
    }
}
