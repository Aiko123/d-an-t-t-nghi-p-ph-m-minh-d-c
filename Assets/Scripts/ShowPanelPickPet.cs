using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPanelPickPet : MonoBehaviour
{
    public GameObject warningPanel; // ??i t??ng WarningPanel trong Canvas
    public GameObject continuePanel; // ??i t??ng ContinuePanel trong Canvas

    // H�m n�y s? ???c g?i khi n�t ???c nh?n
    public void CheckChildren()
    {
        // L?y s? l??ng con c?a ??i t??ng ch?a script n�y
        int childCount = transform.childCount;

        // Ki?m tra s? l??ng con v� hi?n th? b?ng ?i?u khi?n t??ng ?ng
        if (childCount == 0)
        {
            ShowPanel(warningPanel);
            HidePanel(continuePanel);
        }
        else if (childCount == 1)
        {
            ShowPanel(continuePanel);
            HidePanel(warningPanel);
        }
        else // childCount > 1
        {
            ShowPanel(warningPanel);
            HidePanel(continuePanel);
        }
    }

    // Hi?n th? b?ng ?i?u khi?n
    private void ShowPanel(GameObject panel)
    {
        if (panel != null)
        {
            panel.SetActive(true);
        }
    }

    // ?n b?ng ?i?u khi?n
    private void HidePanel(GameObject panel)
    {
        if (panel != null)
        {
            panel.SetActive(false);
        }
    }

    // T�m c�c b?ng ?i?u khi?n trong Canvas khi kh?i ??ng
    private void Start()
    {
        // T�m c�c b?ng ?i?u khi?n trong Canvas
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas != null)
        {
            warningPanel = canvas.transform.Find("WarningPanel")?.gameObject;
            continuePanel = canvas.transform.Find("ContinuePanel")?.gameObject;

            // ?n c? hai b?ng ?i?u khi?n khi kh?i ??ng
            HidePanel(warningPanel);
            HidePanel(continuePanel);
        }
        else
        {
            Debug.LogWarning("Kh�ng t�m th?y Canvas.");
        }
    }
}
