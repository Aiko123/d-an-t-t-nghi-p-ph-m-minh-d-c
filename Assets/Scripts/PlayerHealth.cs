﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    private float health;
    private float lerpTimer;
    public float maxHealth = 100f;
    public float chipSpeed = 2f;
    public Image frontHealthBar;
    public Image backHealthBar;
    public float damageRate = 0.5f; // Tốc độ giảm máu mỗi giây
    public GameObject gameOverUI; // UI GameOver

    private bool isRenamedToRaccoonChecked = false; // Để kiểm tra khi đã đổi tên thành Raccoon

    void Start()
    {
        health = maxHealth;
    }

    void Update()
    {
        // Kiểm tra nếu đối tượng này đã được đổi tên thành "Raccoon"
        if (!isRenamedToRaccoonChecked && gameObject.name == "Raccoon")
        {
            // Tìm đối tượng HealthBar trong Canvas
            GameObject healthBar = GameObject.Find("Canvas/HealthBar");
            if (healthBar != null)
            {
                // Tìm các đối tượng con BackHealthBar và FrontHealthBar trong HealthBar
                Transform backHealthBarTransform = healthBar.transform.Find("BackHealthBar");
                Transform frontHealthBarTransform = healthBar.transform.Find("FrontHealthBar");

                if (backHealthBarTransform != null && frontHealthBarTransform != null)
                {
                    backHealthBar = backHealthBarTransform.GetComponent<Image>();
                    frontHealthBar = frontHealthBarTransform.GetComponent<Image>();
                }
                else
                {
                    Debug.LogWarning("Không tìm thấy BackHealthBar hoặc FrontHealthBar trong HealthBar.");
                }
            }
            else
            {
                Debug.LogWarning("Không tìm thấy HealthBar trong Canvas.");
            }

            // Tìm đối tượng GameOver trong Canvas
            GameObject gameOverObject = GameObject.Find("Canvas/GameOver");
            if (gameOverObject != null)
            {
                gameOverUI = gameOverObject;
                gameOverUI.SetActive(false); // Ẩn UI GameOver
            }
            else
            {
                Debug.LogWarning("Không tìm thấy GameOver trong Canvas.");
            }

            // Đánh dấu đã kiểm tra để không lặp lại việc kiểm tra
            isRenamedToRaccoonChecked = true;
        }

        // Giảm máu theo thời gian
        health -= damageRate * Time.deltaTime;
        health = Mathf.Clamp(health, 0, maxHealth); // Đảm bảo máu không vượt quá giá trị tối đa

        UpdateHealthUI();

        // Kiểm tra nếu máu dưới 2, hiển thị UI GameOver và ngừng toàn bộ Time
        if (health < 2)
        {
            ShowGameOver();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("OnCollisionEnter called");
        // Kiểm tra nếu va chạm với game object có tag là "Food"
        if (collision.gameObject.CompareTag("Food"))
        {
            RestoreHealth(Random.Range(5, 10));
            Destroy(collision.gameObject);
        }
    }

    public void UpdateHealthUI()
    {
        Debug.Log(health);
        float fillF = frontHealthBar.fillAmount;
        float fillB = backHealthBar.fillAmount;
        float hFraction = health / maxHealth;
        if (fillB > hFraction)
        {
            frontHealthBar.fillAmount = hFraction;
            backHealthBar.color = Color.red;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipSpeed;
            percentComplete = percentComplete * percentComplete;
            backHealthBar.fillAmount = Mathf.Lerp(fillB, hFraction, percentComplete);
        }
        if (fillB < hFraction)
        {
            backHealthBar.color = Color.green;
            backHealthBar.fillAmount = hFraction;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipSpeed;
            percentComplete = percentComplete * percentComplete;
            frontHealthBar.fillAmount = Mathf.Lerp(fillF, backHealthBar.fillAmount, percentComplete);
        }
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        lerpTimer = 0f;
    }

    public void RestoreHealth(float healAmount)
    {
        health += healAmount;
        lerpTimer = 0f;
    }

    // Hiển thị UI GameOver và ngừng Time
    void ShowGameOver()
    {
        gameOverUI.SetActive(true); // Hiển thị UI GameOver
        // Time.timeScale = 0; // Ngừng toàn bộ Time
    }
}
