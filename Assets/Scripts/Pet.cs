﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pet : MonoBehaviour
{
    private void Start()
    {
        // Tìm đối tượng PrefabReceiver trong don't destroy on load
        PrefabReceiver prefabReceiver = FindObjectOfType<PrefabReceiver>();

        if (prefabReceiver != null)
        {
            // Đặt PrefabReceiver làm con của đối tượng Pet
            prefabReceiver.transform.SetParent(transform);
            Debug.Log("PrefabReceiver attached to Pet.");
        }
        else
        {
            Debug.LogWarning("PrefabReceiver not found.");
        }
    }
}
