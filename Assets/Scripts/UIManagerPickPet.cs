﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManagerPickPet : MonoBehaviour
{
    public TextMeshProUGUI[] texts; // Mảng chứa các đoạn TextMeshProUGUI cần hiển thị

    // Biến lưu trạng thái của Text, true là đang hiển thị, false là ẩn
    private bool[] textStates;

    private void Start()
    {
        // Khởi tạo mảng trạng thái Text với tất cả các Text đều ẩn
        textStates = new bool[texts.Length];
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].gameObject.SetActive(false);
            textStates[i] = false;
        }
    }

    // Hàm này sẽ được gọi khi bấm vào một nút để hiển thị một Text cụ thể
    public void ShowText(int index)
    {
        // Ẩn tất cả các Text đang hiển thị
        HideAllTexts();

        // Hiển thị Text tương ứng với index
        texts[index].gameObject.SetActive(true);
        textStates[index] = true;
    }

    // Hàm này để ẩn tất cả các Text đang hiển thị
    public void HideAllTexts()
    {
        for (int i = 0; i < texts.Length; i++)
        {
            if (textStates[i])
            {
                texts[i].gameObject.SetActive(false);
                textStates[i] = false;
            }
        }
    }
}
