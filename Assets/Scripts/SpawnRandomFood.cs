﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomFood : MonoBehaviour
{
    public GameObject[] spawnPrefabs; // Mảng chứa các prefabs muốn spawn
    public float spawnRadius = 1f; // Bán kính khoảng cách mà các prefab sẽ được spawn
    public Transform spawnPoint; // Vị trí sẽ spawn các prefab
    private ObjectMovementAndCollision movementScript; // Script xử lý di chuyển và va chạm

    void Start()
    {
        // Lấy và lưu reference đến script xử lý di chuyển và va chạm
        movementScript = GetComponent<ObjectMovementAndCollision>();
    }

    public void SpawnRandomPrefab()
    {
        if (spawnPrefabs.Length == 0)
        {
            Debug.LogWarning("No prefabs assigned to spawn!");
            return;
        }

        // Chọn một prefab ngẫu nhiên từ mảng
        int randomIndex = Random.Range(0, spawnPrefabs.Length);
        GameObject prefabToSpawn = spawnPrefabs[randomIndex];

        // Tạo một vị trí ngẫu nhiên xung quanh spawnPoint
        Vector3 randomOffset = Random.insideUnitSphere * spawnRadius;
        randomOffset.y = 1/2; // Đảm bảo vị trí chỉ cách theo phương ngang
        Vector3 spawnPosition = spawnPoint.position + randomOffset;

        // Spawn prefab tại vị trí ngẫu nhiên đã chọn
        GameObject spawnedPrefab = Instantiate(prefabToSpawn, spawnPosition, Quaternion.identity);

        // Bắt đầu di chuyển đến prefab được spawn
        movementScript.StartMovingToTarget(spawnedPrefab);
    }
}
