﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandle : MonoBehaviour
{
    public GameObject targetGameObject;

    private void Start()
    {
        LoadSavedPrefab();
        InstantiateSavedPrefabAtOrigin();
    }

    private void LoadSavedPrefab()
    {
        string prefabName = PlayerPrefs.GetString("SavedPrefab");
        if (!string.IsNullOrEmpty(prefabName))
        {
            GameObject savedPrefab = Resources.Load<GameObject>(prefabName);
            if (savedPrefab != null)
            {
                Debug.Log("Prefab loaded successfully: " + prefabName);
                ButtonHandle buttonHandler = FindObjectOfType<ButtonHandle>();
                buttonHandler.SetPrefabToReceive(savedPrefab); // Sửa lại thành SetPrefabToReceive
            }
            else
            {
                Debug.LogWarning("Prefab '" + prefabName + "' not found.");
            }
        }
        else
        {
            Debug.Log("No saved prefab found.");
        }
    }

    private void InstantiateSavedPrefabAtOrigin()
    {
        // Không cần sử dụng savedPrefab trong SceneHandler nữa
        // Thay vào đó, prefab đã được lưu từ trước và được truyền từ ButtonHandler
        // Prefab sẽ được instantiate bởi ButtonHandler khi cần thiết
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene("Main");
        Debug.Log("Scene loaded: Main");
    }
}
