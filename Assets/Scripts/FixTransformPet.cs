﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTransformPet : MonoBehaviour
{
    public float decreaseRate = 2f; // Tốc độ giảm giá trị trục Y

    void Start()
    {
        // Giảm giá trị trục Y của vật thể
        transform.position -= new Vector3(0f, decreaseRate * Time.deltaTime, 0f);
    }
}
