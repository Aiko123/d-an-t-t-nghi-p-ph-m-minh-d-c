﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class AnimalManager : MonoBehaviour
{
    public Button toggleButton; // Nút để ẩn/hiện đối tượng
    public GameObject objectSpawner; // GameObject cha chứa các loại đối tượng cần kiểm tra
    public string[] objectNamesToCheck; // Tên của 7 loại GameObject bạn quan tâm

    private bool areObjectsVisible = false; // Trạng thái hiện tại của các đối tượng

    void Start()
    {
        // Thêm sự kiện cho nút button
        toggleButton.onClick.AddListener(ToggleObjectVisibility);
    }

    // Hàm kiểm tra và ẩn/hiện đối tượng khi nhấn nút
    public void ToggleObjectVisibility()
    {
        // Kiểm tra xem objectSpawner có tồn tại không
        if (objectSpawner != null)
        {
            // Duyệt qua các tên GameObject để tìm và ẩn/hiện các đối tượng Quad
            foreach (string objectName in objectNamesToCheck)
            {
                Transform objectToCheck = objectSpawner.transform.Find(objectName);
                if (objectToCheck != null)
                {
                    ToggleQuadVisibility(objectToCheck.gameObject);
                }
                else
                {
                    Debug.Log("Không tìm thấy GameObject có tên " + objectName + " trong " + objectSpawner.name);
                }
            }

            // Đảo ngược trạng thái của các đối tượng
            areObjectsVisible = !areObjectsVisible;
        }
        else
        {
            Debug.Log("objectSpawner không được thiết lập. Vui lòng gán đối tượng cha chứa các loại đối tượng cần kiểm tra.");
        }
    }

    // Hàm ẩn/hiện đối tượng Quad
    private void ToggleQuadVisibility(GameObject obj)
    {
        Transform quadTransform = obj.transform.Find("Quad"); // Tìm đối tượng Quad trong GameObject

        if (quadTransform != null)
        {
            // Đảo ngược trạng thái của đối tượng Quad
            quadTransform.gameObject.SetActive(!areObjectsVisible);

            // Lấy Animator từ đối tượng cha của đối tượng Speak
            Animator anim = quadTransform.GetComponentInParent<Animator>();
            if (anim != null)
            {
                // Đảo ngược trạng thái của biến Speak trong Animator
                bool m_quad = anim.GetBool("Quad");
                anim.SetBool("Quad", !m_quad); // Đặt giá trị mới cho biến Speak trong Animator
            }
        }
        else
        {
            Debug.Log("Không tìm thấy Quad trong " + obj.name);
        }
    }
}
